#include "src/rtklib.h"

void main(){
	gtime_t ts={0},te={0};
	prcopt_t opt=prcopt_default;
	solopt_t sopt=solopt_default;
	filopt_t fopt={0};
	char* infile[]={{"D:\\rtklib\\testdata\\07590920.05o"},
					{"D:\\rtklib\\testdata\\30400920.05o"},
					{"D:\\rtklib\\testdata\\07590920.05n"}};
	char* ofile="D:\\rtklib\\testdata\\mypos.pos";

	opt.navsys=SYS_GPS;
	opt.mode=PMODE_MOVEB;
	sopt.posf=SOLF_ENU;
	postpos(ts,te,0.0,0.0,&opt,&sopt,&fopt,infile,3,ofile,"","");
}